﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library_Management
{
    public class Book
    {
        public Book()
        {

        }
        private int _id;
        private string _title;
        private string _author;
        private bool _avaiable = true;

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Title
        {
            get => _title;
            set => _title = value;
        }

        public string Author
        {
            get => _author;
            set => _author = value;
        }
        public bool Available
        {
            get => _avaiable;
            set => _avaiable = value;
        }

    }
}
