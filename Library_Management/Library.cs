﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library_Management
{
    public class Library
    {
        Book[] books;
        Book[] borrowedBooks;
        int maxStack;
        int top = -1;
        int topBorrowedBook = -1;
        string filePath = @"BookData.txt";
        string fileBorrowedBook = @"BorrowedBookData.txt";
        public Library(int maxStack)
        {
            this.maxStack = maxStack;
            books = new Book[maxStack];
            borrowedBooks = new Book[maxStack];
        }

        public void AddBook(Book book)
        {
            bool isDuplicate = false;
            for (int i = 0; i < top + 1; i++)
            {
                if (book.Id == books[i].Id)
                {
                    isDuplicate = true;
                }
            }
            if (isDuplicate is true)
            {
                throw new Exception("Id is duplicate");
            }
            else
            {
                top++;
                books[top] = book;
            }
        }

        public void AddBorrowedBook(Book book)
        {
            bool isDuplicate = false;
            for (int i = 0; i < topBorrowedBook + 1; i++)
            {
                if (book.Id == borrowedBooks[i].Id)
                {
                    isDuplicate = true;
                }
            }
            if (isDuplicate is true)
            {
                throw new Exception("Id is duplicate");
            }
            else
            {
                topBorrowedBook++;
                borrowedBooks[topBorrowedBook] = book;
            }
        }
        public void CheckOut(int id)
        {
            bool isCheckout = false;
            int length = borrowedBooks.Length;
            for (int i = 0; i < top + 1; i++)
            {
                if (books[i].Id == id && books[i].Available is true)
                {
                    Console.WriteLine("Checkout: " + books[i].Id + " - " + books[i].Title + " - " + books[i].Author);
                    books[i].Available = false;
                    topBorrowedBook++;
                    borrowedBooks[topBorrowedBook] = books[i];
                    isCheckout = true;
                }
            }
            if (isCheckout is true)
            {
                WriteBookData();
                WriteBorrowedBookData();
            }
            if (isCheckout is false)
            {
                throw new Exception("Book is not available");
            }

        }

        public bool ReturnBook(int id)
        {
            bool isBorrowed = true;
            for (int i = 0; i < topBorrowedBook + 1; i++)
            {
                if (borrowedBooks[i].Id == id && borrowedBooks[i].Available is false)
                {
                    borrowedBooks[i].Available = true;
                    top++;
                    books[top] = borrowedBooks[i];
                    return true;
                }
                if (borrowedBooks[i].Available is true)
                {
                    isBorrowed = false;
                }
            }
            if (isBorrowed is false)
            {
                throw new Exception("Book has not been borrowed");
            }
            return false;
        }

        public string PrintBookInfo()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < top + 1; i++)
            {
                if (books[i].Available is true)
                {
                    sb.Append(books[i].Id + " - " + books[i].Title + " - " + books[i].Author + " | ");
                }
            }
            return sb.ToString().Trim();
        }

        public string PrintBookBorrowedInfo()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < topBorrowedBook + 1; i++)
            {
                sb.Append(borrowedBooks[i].Id + " - " + borrowedBooks[i].Title + " - " + borrowedBooks[i].Author + " | ");
            }
            return sb.ToString().Trim();
        }

        public void WriteBookData()
        {
            StringBuilder sb = new StringBuilder();
            if (!File.Exists(filePath))
            {
                File.Create(filePath);
            }
            for (int i = 0; i < top + 1; i++)
            {
                if (books[i].Available is true)
                {
                    sb.Append(books[i].Id + "-" + books[i].Title + "-" + books[i].Author + " | ");
                }
            }
            File.WriteAllText(filePath, sb.ToString().Trim());

        }

        public void WriteBorrowedBookData()
        {
            StringBuilder sb = new StringBuilder();
            if (!File.Exists(fileBorrowedBook))
            {
                File.Create(fileBorrowedBook);
            }
            for (int i = 0; i < topBorrowedBook + 1; i++)
            {
                if (borrowedBooks[i].Available is false)
                {
                    sb.Append(borrowedBooks[i].Id + "-" + borrowedBooks[i].Title + "-" + borrowedBooks[i].Author + " | ");
                }
            }
            File.WriteAllText(fileBorrowedBook, sb.ToString().Trim());

        }

        public void ReadBookData()
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath);
            }
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var book = line.Split('|');
                    for (int i = 0; i < book.Length; i++)
                    {
                        var getBook = book[i];
                        var getSeperator = getBook.IndexOf("-");
                        if (getSeperator != -1)
                        {
                            var id = int.Parse(getBook.Substring(0, getSeperator));
                            int startIndex = getSeperator + 1;
                            getSeperator = getBook.IndexOf('-', startIndex);
                            var title = getBook.Substring(startIndex, getSeperator - startIndex);
                            var author = getBook.Substring(getSeperator + 1);
                            books[i] = new Book { Id = id, Author = author, Title = title, Available = true };
                            AddBook(books[i]);
                        }
                    }
                }
            }
        }

        public void ReadBorrowedBookData()
        {
            if (!File.Exists(fileBorrowedBook))
            {
                File.Create(fileBorrowedBook);
            }
            using (StreamReader reader = new StreamReader(fileBorrowedBook))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var book = line.Split('|');
                    for (int i = 0; i < book.Length; i++)
                    {
                        var getBook = book[i];
                        var getSeperator = getBook.IndexOf("-");
                        if (getSeperator != -1)
                        {
                            var id = int.Parse(getBook.Substring(0, getSeperator));
                            int startIndex = getSeperator + 1;
                            getSeperator = getBook.IndexOf('-', startIndex);
                            var title = getBook.Substring(startIndex, getSeperator - startIndex);
                            var author = getBook.Substring(getSeperator + 1);
                            borrowedBooks[i] = new Book { Id = id, Author = author, Title = title, Available = false };
                            AddBorrowedBook(borrowedBooks[i]);
                        }
                    }
                }
            }
        }
    }
}
