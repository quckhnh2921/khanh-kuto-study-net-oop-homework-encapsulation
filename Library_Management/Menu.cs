﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Library_Management
{
    public class Menu
    {
        public void MenuDisplay()
        {
            Console.WriteLine("----------BOOK-MANAGEMENT----------");
            Console.WriteLine("| 1. Add new book                 |");
            Console.WriteLine("| 2. List of books in library     |");
            Console.WriteLine("| 3. List of borrowed book        |");
            Console.WriteLine("| 4. Borrow book(Enter book id)   |");
            Console.WriteLine("| 5. Return book(Enter book id)   |");
            Console.WriteLine("| 6. Exit                         |");
            Console.WriteLine("-----------------------------------");
        }
    }
}
