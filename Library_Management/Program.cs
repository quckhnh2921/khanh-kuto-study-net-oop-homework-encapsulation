﻿using Library_Management;
Console.OutputEncoding = System.Text.Encoding.Default;
Menu menu = new Menu();
Library library = new Library(100);
int choice = 0;
library.ReadBookData();
library.ReadBorrowedBookData();
do
{
    try
    {
        menu.MenuDisplay();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Book book = new Book();
                Console.WriteLine("Enter book's id:");
                book.Id = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter book's title: ");
                book.Title = Console.ReadLine();
                Console.WriteLine("Enter book's author: ");
                book.Author = Console.ReadLine();
                book.Available = true;
                library.AddBook(book);
                library.WriteBookData();
                break;
            case 2:
                Console.WriteLine(library.PrintBookInfo());
                break;
            case 3:
                Console.WriteLine(library.PrintBookBorrowedInfo());
                break;
            case 4:
                Console.Write("Enter book's id: ");
                int id = int.Parse(Console.ReadLine());
                library.CheckOut(id);
                break;
            case 5:
                Console.Write("Enter book's id to return: ");
                int idReturn = int.Parse(Console.ReadLine());
                var isReturn = library.ReturnBook(idReturn);
                if(isReturn is true)
                {
                    Console.WriteLine("Book has been return");
                }
                else
                {
                    Console.WriteLine("Book id is invalid");
                }
                library.WriteBookData();
                library.WriteBorrowedBookData();
                break;
            case 6:
                Console.WriteLine("Bye bye");
                Environment.Exit(0);
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 6);

